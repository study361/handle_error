## Example to handle error for api response

- https://github.com/gin-gonic/gin
- https://github.com/go-playground/validator

## Goals

- Override status code
- Catching all errors (don't stop app)
- Respond JSON easily


### Structure

```go
type CreateUserRequest struct {
	Email string `json:"email" validate:"required,email"`
	Password string `json:"password" validate:"required"`
}
```

### Response

```go
{
    "errors": {
        "email": "Email must be a valid email address"
    },
    "message": "Some fields are invalid"
}
```
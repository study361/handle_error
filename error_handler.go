package main

import (
	"errors"
	"net/http"
	"reflect"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/locales/en"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	en_translations "github.com/go-playground/validator/v10/translations/en"
)

var (
	validate 	*validator.Validate
	uni 		*ut.UniversalTranslator
)

var ErrNotFound = errors.New("not found")

type ErrorItem map[string] string

type Response struct {
	Errors 		ErrorItem 	`json:"errors"`
	Message 	string 		`json:"message"`
	Data 		any 		`json:"data"`
	Status 		int 		`json:"status"`
}

func ErrorHandler(c *gin.Context) {
	c.Next()

	var data Response
	var errorItem ErrorItem
	var validation validator.ValidationErrors

	data.Status = -1  // -1 for not override status
	if c.Errors == nil {
		return
	}

	en := en.New()
	uni = ut.New(en, en)
	trans, _ := uni.GetTranslator("en")
	en_translations.RegisterDefaultTranslations(validate, trans)

	for _, err := range c.Errors {
		switch {
		case errors.As(err, &validation):
			data.Message = "Some fields are invalid"
			errorItem = make(ErrorItem)

			for _, validationError := range validation {
				errorItem[toSnakeCase(validationError.Field())] = validationError.Translate(trans)
				data.Errors = errorItem
			}
		case errors.Is(err, ErrNotFound):
			data.Message = "Not found"
			data.Errors = ErrorItem{"record": "doesn't exist"}
		default:
			data.Status = http.StatusInternalServerError
			data.Message = reflect.TypeOf(err.Err).String()
		}
	}

	c.JSON(data.Status, gin.H{
		"message": data.Message,
		"errors": data.Errors,
	})
}
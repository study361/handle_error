package main

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

func main() {
	validate = validator.New()

	app := gin.Default()
	app.Use(ErrorHandler)

	app.POST("/", func(ctx *gin.Context) {
		var request CreateUserRequest

		ctx.ShouldBindJSON(&request)
		if err := validate.Struct(request); err != nil {
			validationErrors := err.(validator.ValidationErrors)
			ctx.AbortWithError(http.StatusBadRequest, validationErrors)
			return
		}

		if _, err := findSomething(ctx, 1231231); err != nil {
			ctx.AbortWithError(http.StatusNotFound, err)
			return
		}

		if (ctx.Errors != nil) {
			return
		}

		ctx.JSON(http.StatusOK, gin.H{
			"data": "ok",
		})
	})

	app.Run(":2020")
}

type ExampleRequest struct {
	ValueInt int `json:"value_int" validate:"required,numeric"`
	ValueString string `json:"value_string" validate:"required,alphanum"`
}

type CreateUserRequest struct {
	Email string `json:"email" validate:"required,email"`
	Password string `json:"password" validate:"required"`
}

func findSomething(ctx *gin.Context, id int) (string, error) {
	notFoundError := fmt.Errorf("error on ID: %d: %w", id, ErrNotFound)
	return "", notFoundError
}